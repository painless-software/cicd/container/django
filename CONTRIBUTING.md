# Contributing

We use [Tox](tox.ini) to automate code (re-)generation and execution of
checks and tests.

```console
pip install tox
```

<details>
<summary>Available Tox environments (click to expand)</summary>
<small>

```console
$ tox list
default environments:
generate -> Generate example project and prepare for checks
compose  -> Verify Docker Compose setup
tooling  -> Run MegaLinter using Docker

additional environments:
clean    -> Remove generated project
```

</small>
</details>

## Development

```console
tox -qq
```

This will generate an example project in `.tox/generated` and run its test
suite. You can `cd` into that location and inspect the generated code. To
clean up afterwards, run:

```console
tox run -e clean
```

## Pipeline

Make your changes on a topic branch and open a [merge request][gitlab-mr].

GitLab will run a CI pipeline to [generate a project][gitlabci-generate]
and [verify the generated setup][gitlabci-jobs] (sanity of config files).

[gitlab-mr]: https://gitlab.com/painless-software/cicd/container/django/-/merge_requests
[gitlabci-generate]: .gitlab-ci.yml#L38-45
[gitlabci-jobs]: .gitlab-ci.yml#L47-59
