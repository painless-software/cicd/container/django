# Painless CI/CD Django Project

A Copier template for Django application containerization with modern CI/CD.

## Usage

```console
pip install copier
```

```console
copier copy gl:painless-software/cicd/container/django path/to/your/new/project
```

Once you have put your setup under version control you can pull in changes
from this Copier template using the `update` command:

```console
copier update
```

## Development

See [CONTRIBUTING](CONTRIBUTING.md).

## Background Reading

Some resources that inspired this setup:

- [The best Docker base image for your Python application][pythonspeed] (Itamar Turner-Trauring)
- [Container Image Build Tools: Docker vs. Buildah vs. kaniko/crane][container-tools] (Earthly)
- [A Performance Analysis of Python WSGI Servers: Part 2][wsgi-servers] (AppDynamics)

[pythonspeed]: https://pythonspeed.com/articles/base-image-python-docker-images/
[container-tools]: https://earthly.dev/blog/docker-vs-buildah-vs-kaniko/
[wsgi-servers]: https://www.appdynamics.com/blog/engineering/a-performance-analysis-of-python-wsgi-servers-part-2/
